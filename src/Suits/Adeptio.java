package Suits;

import Tarot.Card;
import Tarot.Suit;

public class Adeptio extends Suit {

	public Adeptio() {
		super();
		
		mCardList.add(new Card("01"," - Ace of Adeptio - Terra.jpg","Image of Holy Terra","Management, control, advancement"));
		mCardList.add(new Card("02"," - Two of Adeptio - The Healer.jpg","Image of a steadfast madicae of the Ministroum","Faith,prayer, community, protection (especially from the Warp or psychic powers),destruction of the unfaithful"));
		mCardList.add(new Card("03"," - Three of Adeptio - The Assassin.jpg","Image of a black-clad Imperial Assassin","Unseen danger, something hidden, sudden death, termination, a simple solution, cleanliness"));
		mCardList.add(new Card("04"," - Four of Adeptio - The Librarian.jpg","Image of a Librarian with clearly visible power","Psykers, psychic power, control, official approval"));
		mCardList.add(new Card("05"," - Five of Adeptio - The Lost.jpg","Image of one of the Adeptus Mechanicus Explorators (or more rarely another kind of Imperial explorer, surveyor or scout).","The unknown, expansion, mysteries"));
		mCardList.add(new Card("06"," - Six of Adeptio - The Servitor.jpg","Image of a Servitor of the Imperium","Legislation, consensus through debate"));
		mCardList.add(new Card("07"," - Seven of Adeptio - The Citizen.jpg","Image of the first citizen among many. This card has many, varied and often hard-to-understand meanings. This card is sometimes called the High the Sigillite","Ceaseless service in the shadow of one greater, great sacrifice, ability over birth, the evolution of Man, rising in rank, rulership"));		
		mCardList.add(new Card("08"," - Eight of Adeptio - The Brute.jpg","Image of a servitor of Mars","Technology, machines, sins of the past, ritual, procedure"));		
		mCardList.add(new Card("09"," - Nine of Adeptio -  The Warrior.jpg","Image of one of the warriors of mankind","The   ultimate   warrior,   victory   against   all   odds,   action,   vitality,  Perfection"));		
		mCardList.add(new Card("10"," - Ten of Adeptio - The Agent.jpg","Image of the Emperor�s faithful on a Crusade","Unflinching loyalty, dominance of lesser men, a favoured son or servant, boundless pride"));
		mCardList.add(new Card("11"," - Jack of Adeptio - The Chaplain.jpg","Image of one of the Space Marine chaplins","Protection, duty, serving a greater purpose"));
		mCardList.add(new Card("12"," - Cavalier of Adeptio - The Commissar.jpg","Image of a Commissar in full formal clothes","Discipline, order, submission to authority, imprisonment, execution"));
		mCardList.add(new Card("13"," - Queen of Adeptio - The Sororitas.jpg","Image of a Sister of Battle, usually in full battle gear. Vanquished foes or objects under their protection may be displayed","Faith, devotion, love, righteous wrath, singularity of purpose"));
		mCardList.add(new Card("14"," - King of Adeptio - The Astartes.jpg","Image of a Space Marine engaged in the work of an Angel of Death � the destruction of the enemies of man","War, crusade, heroes of old, death in battle, excellence, duty"));
		
		mSuitName ="Adeptio";
	}

}
