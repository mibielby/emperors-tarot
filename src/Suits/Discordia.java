package Suits;

import Tarot.Card;
import Tarot.Suit;

public class Discordia extends Suit {

	public Discordia() {
		super();
		
		mCardList.add(new Card("01", " - Ace of Discordia - The Harlequin.jpg", "Image for a (presumably) xenos warrior in strange attire. The Harlequin card is said to be particularly difficult to interpret if it appears in a large spread, for it alters the meaning of many cards most subtly. ", "A hidden enemy, the enemy of my enemy is my ally, unforeseen consequences "));
		mCardList.add(new Card("02", " - Two of Discordia - The Eye of Terror.jpg", "Image of the Eye of Terror. Usually as seen from Cadia, but different views are possible. Some decks depict other lesser-known warp anomalies instead ", "The darkness beyond the Imperium, downfall of man, criminal organizations, dark conspiracies, organized corrption "));
		mCardList.add(new Card("03", " - Three of Discordia - The Warped Renegade.jpg", "Image of an infamous Chaos reaver that has plagued the Imperium since time immemorial", "Discord, sudden anger, jealousy, narrow-mindedness, suspicion, the journey is delayed, work interrupted "));
		mCardList.add(new Card("04", " - Four of Discordia - The Mutant.jpg", "Image of an inhuman mutant. The mutant is easy to identify, for his inner moral weakness has manifested monstrously on the outside. ", "Weakness of character, moral corruption, bodily corruption, outcast "));
		mCardList.add(new Card("05", " - Five of Discordia - The Savage.jpg", "Image of a xenos savage. ", "Danger from within, sedition, attack, traitors, treason "));
		mCardList.add(new Card("06", " - Six of Discordia - The Hedonist.jpg", "Image of a figure that is either a woman or a man or both – it is hard to tell exactly which ", "Depravity, corruption, forbidden knowledge, secrets revealed "));
		mCardList.add(new Card("07", " - Seven of Discordia - The Bringer of Dispair.jpg", "Image of a man, riddles with diseases and covered in sores ", "Sickness, disease,corruption, immortality "));
		mCardList.add(new Card("08", " - Eight of Discordia - The Lord of Battle.jpg", "Image of a man, drenched in blood, standing upon a heap of bodies or skulls. ", "Blood, death, carnage, suffering, pain , murder, torture"));
		mCardList.add(new Card("09", " - Nine of Discordia - The Changer of Ways.jpg", "Image of a man, usually nearly naked, except for a loincloth and/or headdress of coloured feathers ", "Ruin, misfortune, plots, wheels within wheels, confusion, madness "));
		mCardList.add(new Card("10", " - Ten of Discordia - The Misguided.jpg", "Image of a mechanical monstrosity. A reminder that only humans have souls and that machine intelligences may ape humans, but forever be denied true humanity ", "Superficial, unstable, unnatural, faithless, bad news to come "));
		mCardList.add(new Card("11", " - Jack of Discordia - The Traitor.jpg", "Image of Horus, the First Traitor. Horus was an important commander during the Great Crusade who rebelled against the Emperor when the Master of Mankind called him to account for his selfish and corrupt ways ", "Severe, unyielding, strict, intolerance, prejudice, quarrels "));
		mCardList.add(new Card("12", " - Cavalier of Discordia - The Enigma.jpg", "Image of a heinous xenos creature. Usually a ‘sentient’ variety, but sometimes a fierce xenos beast. ", "Danger from without, invasion, attack, something unknown "));
		mCardList.add(new Card("13", " - Queen of Discordia - The Witch.jpg", "Image of a rogue psyker. There are usually overt displays of psychic might ", "The dangers of freedom, unbridled ambition, forbidden knowledge "));
		mCardList.add(new Card("14", " - King of Discordia - The Kraken.jpg", "Image of a foul xenos horde", "Strict, domineering, a jealous and revengeful nature, deceit, infidelity "));
				
		mSuitName = "Discordia";
	}

}
