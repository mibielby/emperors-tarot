package Suits;

import Tarot.Card;
import Tarot.Suit;

public class Executeria extends Suit {
		
	public Executeria() {
		super();
		
		mCardList.add(new Card("01", " - Ace of Executeria - The Nova.jpg", "Image of a perfectly normal ship. You can�t see which ship or where it is but you�re sure you�ve never seen the ship or been in the place", "One man in uncounted humanity, lost identity, a cog in the machine"));
		mCardList.add(new Card("02", " - Two of Executeria - The Navigator.jpg", "Image of one of the Navis Nobilite. He appears human, save for the third eye in his forehead ", "Leadership, decisions, authority "));
		mCardList.add(new Card("03", " - Three of Executeria - The Arbites.jpg", "Image of one of the many soldiers of the Imperium of Man. Perhaps a Guardsman, perhaps a Commissar Cadet, or maybe one of the voidsmen of the Navy ", "Service, duty, sacrifice, the greater good "));
		mCardList.add(new Card("04", " - Four of Executeria - The Emperor's Peace.jpg", "Image of a member of the Missionaria Galaxia, spreading the faith to the heathen ", "The holy word, the supremacy of man, the Imperial truth"));
		mCardList.add(new Card("05", " - Five of Executeria - The Navis Nobilite.jpg", "Image of a member of the ruling class ", "Good fortune, well-being, head of the family, head of the household, someone to be obeyed, the way of things "));
		mCardList.add(new Card("06", " - Six of Executeria - The Victory.jpg", "Image of a ranking officer, such as a General of Guards or Admiral of the Navy. ", "Planning, intelligence, the greater picture "));
		mCardList.add(new Card("07", " - Seven of Executeria - The Contract.jpg", "Image of a Rogue Trader in all his splendour. This card has many meanings. It has no reverse image, but many variations ", "A headlong rush into life, a strong man, bravery, a skillful and clever person, an unexpected coming or going of a matter, a troublemaker, a crafty and secretive person "));
		mCardList.add(new Card("08", " - Eight of Executeria - The Duel.jpg", "Image of an officer fighting the enemy", "Command, giving orders, an intermediary "));
		mCardList.add(new Card("09", " - Nine of Executeria - The Fabricator.jpg", "Image of an Adeptus Mechanicus Fabricator (factory manager). ", "Efficiency, transformation, vigilance, A stern master, no room for error"));
		mCardList.add(new Card("10", " - Ten of Executeria - The Struggle.jpg", "Image of the war machine of Adeptus Astartes ", "A quick and confident decision, cruelty out of necessity, one who can bear their sorrow, narrow-mindedness, an obstinate person "));
		mCardList.add(new Card("11", " - Jack of Executeria - The Envoy.jpg", "Image of a Lord Militant of the Imperium ", "A judge, a powerful commander, a firm friendship,some who is cautious but confident, a wise counsellor, willingness to sacrifice the lives of others, to do what must be done "));
		mCardList.add(new Card("12", " - Cavalier of Executeria - The Magus.jpg", "Image of an Arch-Magos of Mars ", "Mastery of a craft, machines, old ways "));
		mCardList.add(new Card("13", " - Queen of Executeria - The Watcher.jpg", "Image of the Captain of an Imperial Naval vessel. Alternatively it could be another ranking officer, such as a regimental commander of Guards (the meaning of Captain is leader of men )", "Ultimate mastery over a limited area, a means to an end, loss is acceptable, failure is not "));
		mCardList.add(new Card("14", " - King of Executeria - The Astronomicon.jpg", "Image of a Paternova of the Navis Nobilite ", "Diplomacy, negotiation, a breed apart "));
				
		mSuitName = "Executeria";
	}
}
