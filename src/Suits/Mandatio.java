package Suits;

import Tarot.Card;
import Tarot.Suit;

public class Mandatio extends Suit {

	public Mandatio() {
		super();
		
		mCardList.add(new Card("01", " - Ace of Mandatio - The Hive.jpg", "Image of a �typical� Imperial citizens �can vary a lot, but the symbology is mostly the same ", "Balance, foundation, stability, infinity/something large "));
		mCardList.add(new Card("02", " - Two of Mandatio - The Ledge.jpg","Image of a savage man:A savage is a human who is not part of the Imperium or have heard of the God-Emperor. Those who reject him or oppose him are heretics instead. ", "Outsiders, something different, loneliness, loss, lack of respect, lack of civilization ")); 
		mCardList.add(new Card("03", " - Three of Mandatio - The Governor.jpg", "Image of an Imperial Governor", "Mother/father figure, stern but fair, between the rock and hard place"));
		mCardList.add(new Card("04", " - Four of Mandatio - The Enforcer.jpg", "Image of a non-commissioned officer or equivalent. A veteran Guardsman sergeant for example ", "Lifelong service, resolute action, execution of orders "));
		mCardList.add(new Card("05", " - Five of Mandatio - The Tithe.jpg", "Image of a tithe from one of the great commercial houses", "Hoarding, gratification, wealth, trade, social mobility "));
		mCardList.add(new Card("06", " - Six of Mandatio - The Adept.jpg", "Image of a craftsman. The type of craft displayed has a bearing on the exact meaning of the card ", "Learning a trade or profession, employment is coming soon, skill, handiwork, small money gain "));
		mCardList.add(new Card("07", " - Seven of Mandatio - The Schola Progenia.jpg", "Image a Speaker for the Chartist Captains. The speaker represents the commercial vessels that bind the Imperium together ", "Travel, strength through unity, interdependency "));
		mCardList.add(new Card("08", " - Eight of Mandatio - The Administratum.jpg", "Image of an Imperial double eagle", "Effort and hard work will cause growth, a pause during development, re-evaluations "));
		mCardList.add(new Card("09", " - Nine of Mandatio - The Cogitator.jpg", "Image of a Cogitator of the Administratum ", "Height of professionalism, difficult choices, numerous tasks to be completed "));
		mCardList.add(new Card("10", " - Ten of Mandatio - The Scribe.jpg", "Image of a lowly Administratum scribe, busy with his work", "Duty, diligence, repetition"));
		mCardList.add(new Card("11", " - Jack of Mandatio - The High Priest.jpg", "Image of a Cardinal of the Adeptus Ministorum ", "The importance of faith, dogma and ritual"));
		mCardList.add(new Card("12", " - Cavalier of Mandatio - The Inquisitor.jpg", "Image of an Inquisitor of the Holy Ordos, his Rosette clearly displayed. His mien is fearsome and his authority absolute", "Authority, fear, secrecy, false appearances, agents of the Throne, Exterminatus"));
		mCardList.add(new Card("13", " - Queen of Mandatio - The Cannoness.jpg", "Image of a church elder, such as a bishop of the Imperial Creed ", "The wisdom of your elders, the Imperial truth, the sanctity of man "));
		mCardList.add(new Card("14", " - King of Mandatio - The Omnissiah.jpg", "Image of the High Lord of Mars. ", "A wise ruler, guidance, planning for the future, council, closeness to the Emperor "));
				
		mSuitName = "Mandatio";
	}

}
