package Tarot;


public class Card {

	private String mNumber;
	private String mName;
	private String mStandard;
	private String mDescription;
	private String mVariation;
	private String mReverse;
	
	
	public Card(String pNumber, String pName) {
		mNumber 	 = pNumber;
		mName 		 = pName;
		mDescription = "";
		mStandard  	 = "";
		mVariation	 = "";
		mReverse 	 = "";
	}
	
	public Card(String pNumber, String pName,
			String pDesc, String pStandard, String pVariation, String pReverse) {
		mNumber 	= pNumber;
		mName 		= pName;
		mStandard 	= pStandard;
		mVariation	= pVariation;
		mReverse 	= pReverse;
		mDescription= pDesc;
	}
	
	public Card(String pNumber, String pName,
			String pDesc, String pStandard) {
		mNumber 	= pNumber;
		mName 		= pName;
		mDescription= pDesc;
		mStandard 	= pStandard;
		mVariation	= "";
		mReverse 	= "";
	}

	public String getNumber() {
		return mNumber;
	}

	public String getName() {
		return mName;
	}

	public String getStandard() {
		return mStandard;
	}

	public String getVariation() {
		return mVariation;
	}

	public String getReverse() {
		return mReverse;
	}
	
	public String getDescription() {
		return mDescription;
	}

	
}
