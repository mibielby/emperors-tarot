package Tarot;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;

import javax.swing.Icon;
import javax.swing.JLabel;

public class ReversedLabel extends JLabel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ReversedLabel () { super(); }
	public ReversedLabel (Icon image) {super (image);}
	public ReversedLabel (Icon image, int align) {super (image, align);}
	public ReversedLabel (String text) { super (text);}
	public ReversedLabel (String text, Icon icon, int align) {
		super (text, icon, align); 
	} 
	public ReversedLabel (String text, int align) { super (text, align);}
	
	public void paint (Graphics g) {
		if (g instanceof Graphics2D) {
			Graphics2D g2 = (Graphics2D) g;
			AffineTransform flipTrans = new AffineTransform();
			flipTrans.setToTranslation (getWidth(), getHeight());
			flipTrans.scale (-1.0, -1.0);
			g2.transform (flipTrans);
			super.paint(g);
		} else { 
			super.paint(g); 
		} 
	} 

}
