package Tarot;

import java.security.SecureRandom;
import java.util.ArrayList;

public class Suit {

	protected ArrayList<Card> mCardList;
	protected String mSuitName;
	
	public Suit() {
		mCardList = new ArrayList<Card>();
	}
	
	public Card getRandomCard(){
		SecureRandom aRandom = new SecureRandom();
		return mCardList.get(aRandom.nextInt(mCardList.size()));
	}
	
	public Card getCard(int i){
		return mCardList.get(i);
	}

	public String getSuitName() {
		return mSuitName;
	}
	
}
