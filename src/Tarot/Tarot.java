package Tarot;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class Tarot {

	
	
	private static void showImage(String path){
		JFrame frame = new JFrame();
		ImageIcon icon = new ImageIcon(path);
		JLabel label = new JLabel(icon);
		frame.add(label);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.pack();
		frame.setVisible(true);
	}
	
	 public static void main(String args[]) {
		 
		JFrame frame = new JFrame();

		if (args.length == 15){
			// Suit, Card, Suit, Card ...
			ArrayList<Integer> aSuitList = new ArrayList<Integer>();
			ArrayList<Integer> aCardList = new ArrayList<Integer>();
			ArrayList<Boolean> aUpright = new ArrayList<Boolean>();
			for (int i=0; i<5; i++){
				aSuitList.add(Integer.valueOf(args[i*3]));
				aCardList.add(Integer.valueOf(args[i*3+1]));
				aUpright.add(Boolean.valueOf(args[i*3+2]));
			}
			frame.add(new TarotReading(aSuitList, aCardList, aUpright));
		} else {
			frame.add(new TarotReading());
		}
		
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
	        
	 }
}