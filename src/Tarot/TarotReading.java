package Tarot;
import java.security.SecureRandom;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import Suits.Adeptio;
import Suits.Arcane;
import Suits.Discordia;
import Suits.Executeria;
import Suits.Mandatio;

public class TarotReading extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	static String mCardFolder = "Tarot/";
	
	public TarotReading() {
		
		ArrayList<Suit> aDeck = new ArrayList<Suit>();

		aDeck.add(new Adeptio());
		aDeck.add(new Arcane());
		aDeck.add(new Discordia());
		aDeck.add(new Executeria());
		aDeck.add(new Mandatio());
		
		SecureRandom aRandom = new SecureRandom();

		for (int i=0; i<5; i++){
			Suit aSuit = aDeck.get(aRandom.nextInt(aDeck.size()));
			Card aCard = aSuit.getRandomCard();
			String aImageString = mCardFolder + aSuit.getSuitName() + "/" + aCard.getNumber() + aCard.getName();
			
			String nameString = aCard.getName();
			int aIdx = nameString.indexOf("-");
			if (aIdx != -1){
				nameString = nameString.substring(aIdx+1);
			}
			nameString = nameString.substring(0, nameString.indexOf("."));
			
			String printString = nameString + "\n "  + aCard.getDescription();
			if (!aCard.getVariation().isEmpty()){
				printString += " (variations: " + aCard.getVariation() + ")";
			}
			
			ImageIcon aIcon = new ImageIcon(aImageString);
			if (aRandom.nextBoolean()){
				JLabel aLabel = new JLabel(aIcon);
				add(aLabel);
				printString += "\n " + aCard.getStandard();
			} else {
				JLabel aLabel = new ReversedLabel(aIcon);
				add(aLabel);
				printString += "\n Reversed :: " + aCard.getStandard();
			}
			
			System.out.println(printString);
			System.out.println("###");
		}

	}
	
	public TarotReading(ArrayList<Integer> pSuits, ArrayList<Integer> pCards, ArrayList<Boolean> pUpright) {
		
		if (pSuits.size() != 5 || pCards.size() != 5 || pUpright.size() != 5){
			System.err.println("Not enough suits or cards!");
			System.exit(1);
		}
		
		ArrayList<Suit> aDeck = new ArrayList<Suit>();

		aDeck.add(new Adeptio());
		aDeck.add(new Arcane());
		aDeck.add(new Discordia());
		aDeck.add(new Executeria());
		aDeck.add(new Mandatio());
		
		SecureRandom aRandom = new SecureRandom();

		for (int i=0; i<5; i++){
			Suit aSuit = aDeck.get(pSuits.get(i));
			Card aCard = aSuit.getCard(pCards.get(i));
			String aImageString = mCardFolder + aSuit.getSuitName() + "/" + aCard.getNumber() + aCard.getName();
			
			String nameString = aCard.getName();
			int aIdx = nameString.indexOf("-");
			if (aIdx != -1){
				nameString = nameString.substring(aIdx+1);
			}
			nameString = nameString.substring(0, nameString.indexOf("."));
			
			String printString = nameString + "\n "  + aCard.getDescription();
			if (!aCard.getVariation().isEmpty()){
				printString += " (variations: " + aCard.getVariation() + ")";
			}
			
			ImageIcon aIcon = new ImageIcon(aImageString);
			if (pUpright.get(i)){
				JLabel aLabel = new JLabel(aIcon);
				add(aLabel);
				printString += "\n " + aCard.getStandard();
			} else {
				JLabel aLabel = new ReversedLabel(aIcon);
				add(aLabel);
				printString += "\n Reversed :: " + aCard.getStandard();
			}
			
			System.out.println(printString);
			System.out.println("###");
		}

	}

}
